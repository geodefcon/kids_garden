from rest_framework import viewsets
from rest_framework import mixins

from api.serializers import ChildSerializer, LogSerializer
from api.models import Child, Log


class CreateListRetrieveViewSet(mixins.CreateModelMixin,
                                mixins.ListModelMixin,
                                mixins.RetrieveModelMixin,
                                viewsets.GenericViewSet):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.
    """
    pass


class ChildView(viewsets.ModelViewSet):
    queryset = Child.objects.filter(attends=True)
    serializer_class = ChildSerializer


class LogView(CreateListRetrieveViewSet):
    # Now we cannot delete or change Log entries
    queryset = Log.objects.all()
    serializer_class = LogSerializer


