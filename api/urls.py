from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from api.views import ChildView, LogView


router = routers.DefaultRouter()
router.register(r'children', ChildView)
router.register(r'log', LogView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
