from django.db import models


class Child(models.Model):
    name = models.CharField(max_length=200)
    birthday = models.DateField()
    garden_class = models.CharField(max_length=100)
    attends = models.BooleanField(default=True)
    photo = models.ImageField(upload_to='', blank=True)
    GENDER_CHOICES = (
        ('B', 'Boy'),
        ('G', 'Girl'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)


class Log(models.Model):
    child = models.ForeignKey('Child', on_delete=models.CASCADE, related_name='log_entries')
    time = models.TimeField(auto_now_add=True)
    date = models.DateField(auto_now_add=True)
    PARENT_CHOICES = (
        ('M', 'Mother'),
        ('F', 'Father'),
    )
    parent = models.CharField(max_length=1, choices=PARENT_CHOICES)
    WHERE_CHOICES = (
        ('I', 'In'),
        ('O', 'Out'),
    )
    where = models.CharField(max_length=1, choices=WHERE_CHOICES)



