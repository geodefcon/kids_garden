import tempfile
import json

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from django.test import TestCase, override_settings
from rest_framework.test import APIClient
from rest_framework import status


MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class ChildTestCase(TestCase):
    def setUp(self):
        self.image = SimpleUploadedFile(name='test_image.jpg', content=open('api/tests/test_photo.jpg', 'rb').read(),
                                       content_type='image/jpeg')
        data = {
            'name': 'Dima',
            'birthday': '2000-05-30',
            'garden_class': 'B1',
            'attends': True,
            'photo': self.image,
            'gender': 'B',
        }
        response = self.client.post(reverse('child-list'), format='multipart', data=data)

    def test_create_log_entry(self):
        response = self.client.get(reverse('child-list'))
        response_data = json.loads(response.content)
        id = response_data[0]['id']

        data = {
            'child': id,
            'where': 'I',
            'parent': 'M',
        }
        response = self.client.post(reverse('log-list'), format='multipart', data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_cannot_edit_log_entry(self):
        response = self.client.get(reverse('child-list'))
        response_data = json.loads(response.content)
        child_id = response_data[0]['id']

        data = {
            'child': child_id,
            'where': 'I',
            'parent': 'M',
        }
        # set new entry
        response = self.client.post(reverse('log-list'), format='multipart', data=data)
        response_data = json.loads(response.content)
        log_id = response_data['id']

        # try put new data
        data = {
            'child': child_id,
            'where': 'I',
            'parent': 'F',
        }
        client = APIClient()
        client.put('/api/log/' + str(log_id) + '/', data, format='multipart')

        #  check what we still have old data
        response = self.client.get('/api/log/' + str(log_id) + '/')
        response_data = json.loads(response.content)
        self.assertEqual(response_data['parent'], 'M')


    def test_cannot_delete_log_entry(self):
        response = self.client.get(reverse('child-list'))
        response_data = json.loads(response.content)
        child_id = response_data[0]['id']

        data = {
            'child': child_id,
            'where': 'I',
            'parent': 'M',
        }
        # set new entry
        response = self.client.post(reverse('log-list'), format='multipart', data=data)
        # try delete
        client = APIClient()
        client.delete('/api/log/1/')

        #  check what we still have old data
        response = self.client.get('/api/log/1/')
        response_data = json.loads(response.content)
        self.assertEqual(response_data['parent'], 'M')