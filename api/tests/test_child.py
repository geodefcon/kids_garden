import tempfile
import json

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from django.test import TestCase, override_settings
from rest_framework.test import APIClient
from rest_framework import status


MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class ChildTestCase(TestCase):
    def setUp(self):
        self.image = SimpleUploadedFile(name='test_image.jpg', content=open('api/tests/test_photo.jpg', 'rb').read(),
                                       content_type='image/jpeg')

    def test_create_child(self):
        data = {
            'name': 'Vera',
            'birthday': '2000-05-30',
            'garden_class': 'B1',
            'attends': True,
            'photo': self.image,
            'gender': 'B',
        }
        response = self.client.post(reverse('child-list'), format='multipart', data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_child_without_name(self):
        data = {
            'birthday': '2000-05-30',
            'garden_class': 'B1',
            'attends': True,
            'photo': self.image,
            'gender': 'B',
        }
        response = self.client.post(reverse('child-list'), format='multipart', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_show_only_attended(self):
        data = {
            'name': 'Attended',
            'birthday': '2000-05-30',
            'garden_class': 'B1',
            'attends': True,
            'photo': self.image,
            'gender': 'B',
        }
        response = self.client.post(reverse('child-list'), format='multipart', data=data)

        data = {
            'name': 'not_Attended',
            'birthday': '2000-05-30',
            'garden_class': 'B1',
            'attends': False,
            'gender': 'B',
        }
        response = self.client.post(reverse('child-list'), format='multipart', data=data)
        # look at the result
        response = self.client.get(reverse('child-list'))
        response_data = json.loads(response.content)
        self.assertEqual(len(response_data), 1)

    def test_edit_child_name(self):
        # save two children
        data = {
            'name': 'Vera_1',
            'birthday': '2000-05-30',
            'garden_class': 'B1',
            'attends': True,
            'photo': self.image,
            'gender': 'G',
        }
        response = self.client.post(reverse('child-list'), format='multipart', data=data)

        data = {
            'name': 'Vera_2',
            'birthday': '2000-05-30',
            'garden_class': 'B1',
            'attends': True,
            'gender': 'B',
        }
        response = self.client.post(reverse('child-list'), format='multipart', data=data)

        # change the name of one of them
        data = {
            'name': 'Vera_3',
            'birthday': '2000-05-30',
            'garden_class': 'B1',
            'attends': True,
            'gender': 'B',
        }
        client = APIClient()
        client.put('/api/children/2/', data, format='multipart')

        # check
        response = self.client.get('/api/children/2/')
        response_data = json.loads(response.content)
        self.assertEqual(response_data['name'], 'Vera_3')


